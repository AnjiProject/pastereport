===========
PasteReport
===========

:Project version: v0.7.1

Simple python async project to store html or text reports.
Uses RethinkDB to store information about reports.

Getting Started
---------------

Just run pastereport with rethinkdb via docker

.. code:: bash

    docker run --net host -d --name rethinkdb rethinkdb
    docker run --net host -d --name pastereport registry.gitlab.com/anjiproject/pastereport:v0.7.1

Than paste report via curl

.. code:: bash

    curl -X POST --data @reportname http://127.0.0.1:8877/paste/html

And receive a link to report.


Advanced usage
--------------

Every report has group and name. You can specify group and name via url path

.. code:: bash

    curl -X POST --data @reportname http://127.0.0.1:8877/paste/html/report_group
    curl -X POST --data @reportname http://127.0.0.1:8877/paste/html/report_group/report_name

Also, you can view all group and reports on the main page at http://127.0.0.1:8877


Sentry support
--------------

To use sentry, please install modules :code:`raven, raven-aiohttp, sanic_sentry`.
That before running app setup environment variable :code:`SENTRY_DSN`.

Migration script
----------------

To update database, if you use version before v0.7.1, please, use script


.. code:: javascript

    r.db('test').table('reports').filter(r.row.hasFields('__python_info').not()).update({"__python_info": {
        "class_name": "Report" ,
        "module_name": "pastereport.models"
    }})