import os

from sanic import Sanic, response
from sanic.exceptions import NotFound
from sanic.request import Request
from sanic.response import file_stream, HTTPResponse

from sanic_prometheus import monitor
from sanic_service_utils.common import (
    anji_orm_configuration, aiohttp_session_configuration,
    log_configuration, jinja_session_configuration, sentry_configuration, sanic_session_configuration
)
from sanic_service_utils.session_interfaces.rethinkdb import RethinkDBSessionInterface
from sanic_oauth.blueprint import login_required, oauth_blueprint
from sanic_openapi import doc, swagger_blueprint, openapi_blueprint


from .api import report_api, paste_api, report_api_v2

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.7.1"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"
STATIC_FOLDER_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'static')

app = Sanic('pastereport')
app.session_interface = RethinkDBSessionInterface()
app.blueprint(report_api)
app.blueprint(report_api_v2)
app.blueprint(paste_api)
app.blueprint(anji_orm_configuration)
app.blueprint(aiohttp_session_configuration)
app.blueprint(log_configuration)
app.blueprint(jinja_session_configuration)
app.blueprint(sentry_configuration)
app.blueprint(oauth_blueprint)
app.blueprint(sanic_session_configuration)
app.blueprint(swagger_blueprint)
app.blueprint(openapi_blueprint)

monitor(app).expose_endpoint()

app.static('/', STATIC_FOLDER_PATH)


app.config.API_VERSION = __version__
app.config.API_TITLE = 'PasteReport'
app.config.API_DESCRIPTION = 'PasteReport REST API'
app.config.API_PRODUCES_CONTENT_TYPES = ['application/json']
app.config.API_CONTACT_EMAIL = __email__


@app.route('/')
@app.exception(NotFound)
@doc.exclude(True)
@login_required(add_user_info=False)
async def redirect_to_index(request: Request, exception=None) -> HTTPResponse:  # pylint: disable=unused-argument
    return await file_stream(os.path.join(STATIC_FOLDER_PATH, 'index.html'))


@app.route('/api/about')
async def about(_request: Request) -> response.HTTPResponse:
    return response.json({
        'name': 'PasteReport',
        'version': __version__,
        'license': __license__
    })
