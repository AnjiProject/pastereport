from typing import List, Dict, Any

from anji_orm import StringField, DatetimeField, QueryAst
from sanic_service_utils.orm_utils import OpenAPIDescriptionMixin, DataTableMixin
from sanic.request import Request

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.7.1"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"


class Report(OpenAPIDescriptionMixin, DataTableMixin):

    _table = 'reports'

    group_name = StringField(secondary_index=True)
    report_type = StringField()
    file_name = StringField()
    file_url = StringField()
    report_date = DatetimeField()

    @classmethod
    async def get_group_list(cls) -> List[Dict[str, Any]]:
        counter = await Report.all().group(Report.group_name).count().async_run()
        return [{"name": key, "size": value} for key, value in counter.items()]

    @classmethod
    def process_before_filter(cls, db_request: QueryAst, request: Request) -> QueryAst:
        if request.args.get('reportGroup'):
            db_request = QueryAst & Report.group_name.eq(request.args.get('reportGroup'))
        return db_request
