from sanic import Blueprint
from sanic.request import Request
from sanic.response import json, HTTPResponse

from sanic_openapi import doc

from ..models import Report

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.7.1"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"


__all__ = ['report_api', 'report_api_v2']

report_api = Blueprint("Report Api", url_prefix='/api')  # pylint: disable=invalid-name
report_api_v2 = Blueprint("Report Api V2", url_prefix='/api/v2')  # pylint: disable=invalid-name


@report_api.route('/reports')
@report_api.route('/reports/<group>')
@report_api.route('/reports/<group>/<name>')
@doc.exclude(True)
async def api_get_reports_list(_request: Request, _group: str = None, _name: str = None) -> None:
    # Just dummy api for anji bot
    return json({})


@report_api_v2.route('/reports')
@doc.summary("List of report")
@doc.consumes(
    {"pageIndex": doc.String("Index of current page")},
    {"sortDirection": doc.String("Direction for sorting", choices=('desc', 'asc'))},
    {"pageSize": doc.Integer("Size for page")},
    {"sortColumn": doc.String("Column for sorting")},
    {"reportGroup": doc.String("Report group to filter")},
)
@doc.route(produces=[Report.get_full_description()])
async def api_get_reports_list_v2(request: Request) -> HTTPResponse:
    return await Report.process_web_request(request, primary_key='file_name', prettify_response=False)


@report_api_v2.route('/report/groups')
async def get_report_groups(_request: Request) -> HTTPResponse:
    return json(await Report.get_group_list())
