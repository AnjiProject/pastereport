import uuid
import os
from datetime import datetime

import aiofiles
from prometheus_client import Counter
import uvloop
import rethinkdb as R
from sanic import Blueprint, Sanic
from sanic.request import Request
from sanic.response import HTTPResponse, text, file
from sanic_oauth.blueprint import login_required
from sanic_openapi import doc

from ..models import Report

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.7.1"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"

__all__ = ['paste_api']

paste_api = Blueprint('Paste Api')  # pylint: disable=invalid-name


@paste_api.listener('before_server_start')
async def setup_mertices(sanic_app: Sanic, _loop: uvloop.Loop) -> None:
    sanic_app.upload_metric = Counter(
        'pastereport_loaded_reports',
        'Count of loaded reports',
        ['group']
    )
    sanic_app.config.setdefault('DEFAULT_GROUP', 'Отчеты')
    sanic_app.config.setdefault('DEFAULT_NAME', 'Отчет')
    sanic_app.config.setdefault('UPLOAD_DIRECTORY', './data')
    if not os.path.exists(sanic_app.config.UPLOAD_DIRECTORY):
        os.mkdir(sanic_app.config.UPLOAD_DIRECTORY)


@paste_api.route('/paste/<report_type>', methods=['POST', 'PUT'])
@paste_api.route('/paste/<report_type>/<group>', methods=['POST', 'PUT'])
@paste_api.route('/paste/<report_type>/<group>/<name>', methods=['POST', 'PUT'])
async def paste(
        request: Request, report_type: str,
        group: str = None, name: str = None) -> HTTPResponse:
    file_uuid = str(uuid.uuid4())
    full_path = os.path.join(request.app.config.UPLOAD_DIRECTORY, file_uuid)
    if group is None:
        group = request.app.config.DEFAULT_GROUP
    if name is None:
        name = request.app.config.DEFAULT_NAME
    async with aiofiles.open(full_path, mode='w') as target_file:
        await target_file.write(request.body.decode('utf-8'))
    request.app.upload_metric.labels(group).inc()  # pylint: disable=no-member
    report = Report(
        group_name=group,
        report_type=report_type,
        file_name=name,
        file_url='/{report_type}/{file_uuid}'.format(
            report_type=report_type,
            file_uuid=file_uuid
        ),
        report_date=datetime.now(R.make_timezone("00:00"))
    )
    await report.async_send()
    return text(f'{request.scheme}://{request.host}/{report_type}/{file_uuid}')


@paste_api.route('/html/<filename>', methods=['GET'])
@login_required(add_user_info=False)
@doc.exclude(True)
async def get_html(request: Request, filename: str) -> HTTPResponse:
    return await file(
        os.path.join(request.app.config.UPLOAD_DIRECTORY, filename),
        mime_type='text/html'
    )


@paste_api.route('/text/<filename>', methods=['GET'])
@login_required(add_user_info=False)
@doc.exclude(True)
async def get_text(request: Request, filename: str) -> HTTPResponse:
    return await file(
        os.path.join(request.app.config.UPLOAD_DIRECTORY, filename),
        mime_type='text/plain'
    )
