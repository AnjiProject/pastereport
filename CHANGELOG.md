# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.7.1] - 2018-06-17

### Changed

- 'anji_orm' versino to `v0.5.4`

## [0.7.0] - 2018-05-18

### Added

- `sanic-server-utils` library
- `manage.py` as util file
- oauth implementation
- swagger doc support

### Changed

- Migrate to new anjiORM version
- Migrate to fixed sanic-prometheus version
- Now report name just uuid4 string
- Base project structure migrated to new sanic model

### Removed

- base58 code
- Old api support (just dummy response)

## [0.6.1] - 2017-12-18

### Added

- Automatic builds

### Fixed

- Correct task cleanup
- Dependencies

## [0.6.0] - 2017-12-10

### Changed

- Use ORM instead of pure rethinkdb usage

## [0.5.3] - 2017-12-04

### Fixed

- Fix some mypy hints
- UPLOAD_DIRECTORY env variable usage

## [0.5.2] - 2017-12-03

### Added

- Optional sentry support

[Unrelesed]: https://gitlab.com/AnjiProject/pastereport/compare/v0.7.1...HEAD
[0.7.1]: https://gitlab.com/AnjiProject/pastereport/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.com/AnjiProject/pastereport/compare/v0.6.1...v0.7.0
[0.6.1]: https://gitlab.com/AnjiProject/pastereport/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/AnjiProject/pastereport/compare/v0.5.3...v0.6.0
[0.5.3]: https://gitlab.com/AnjiProject/pastereport/compare/v0.5.2...v0.5.3
[0.5.2]: https://gitlab.com/AnjiProject/pastereport/compare/v0.5.1...v0.5,2