import asyncio
import aiohttp
import uvloop


asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

with open('templates/index.html') as test_file:
    test_data = ("\n".join(test_file.readlines())).encode('utf-8')

async def single_load_test(index: int) -> None:
    async with aiohttp.ClientSession() as session:
        async with session.post(f'http://127.0.0.1:8877/paste/html/load_test/{index}', data=test_data) as resp:
            print(await resp.text())

async def load_test() -> None:
    for i in range(0, 1000):
        await single_load_test(i)


loop: asyncio.AbstractEventLoop = asyncio.get_event_loop()
loop.run_until_complete(load_test())
