import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { MatPaginator, MatSort } from '@angular/material';

import { ReportService } from '../report.service'

@Component({
  selector: 'app-report-group',
  templateUrl: './report-group.component.html',
  styleUrls: ['./report-group.component.css']
})
export class ReportGroupComponent implements OnInit {

  private route: ActivatedRoute;
  private location: Location;
  private reportGroup: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['file_name', 'report_type', 'report_date'];

  stockColumnDescription = {
    report_type: 'Report Type',
    file_name: 'Report name',
    report_date: "Report date",
  }

  constructor(private dataSource: ReportService, route: ActivatedRoute, location: Location) {
      this.route = route;
      this.location = location;
      this.reportGroup = this.route.snapshot.paramMap.get('name');
  }

  ngOnInit() {
  }

  getDataSource() {
      return this.dataSource;
  }

  updateData() {
      this.dataSource.updateData()
  }

  ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.reportGroup = this.reportGroup;
      this.dataSource.load()
  }

  applyFilter(filterValue: string) {
      filterValue = filterValue.trim();
      this.dataSource.filter = filterValue;
  }

}
