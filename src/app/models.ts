export interface Report {
    group_name: string;
    report_type: string;
    file_name: string;
    file_url: string;
    report_date: string;
}