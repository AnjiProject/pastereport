import { Injectable } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { CollectionViewer } from "@angular/cdk/collections/typings/collection-viewer"
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { MatPaginator, MatSort } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';


@Injectable()
export abstract class AbstractApiDataSource<T> implements DataSource<T> {

    private _data: BehaviorSubject<T[]>;
    private _filter: string;
    private _loaded: boolean;
    private _sortColumn: string;
    private _sortDirection: string;
    private _apiPath: string = null;
    protected http: HttpClient;

    paginator: MatPaginator;
    sort: MatSort;

    set filter(filter: string) {
        this._filter = filter;
        this.updateData();
    }
    get filter(): string { return this._filter; }

    abstract getApiPath(): string

    constructor(http: HttpClient) {
        this._data = new BehaviorSubject([]);
        this._loaded = false;
        this._filter = null;
        this._sortColumn = null;
        this._sortDirection = null;
        this._apiPath = this.getApiPath()
        this.http = http;
    }

    additionalParamsProcessing(params: HttpParams): HttpParams {
        return params;
    }

    additionalDataProcessing(data: [T]): [T] {
        return data;
    }

    updateData() {
        let params = new HttpParams()
            .set('pageIndex', this.paginator.pageIndex.toString())
            .set('pageSize', this.paginator.pageSize.toString())
        if (this._filter) {
            params = params.set('filter', this._filter);
        }
        if (this._sortDirection) {
            params = params.set('sortDirection', this._sortDirection)
                .set('sortColumn', this._sortColumn)
        }
        params = this.additionalParamsProcessing(params)
        this.http.get(this._apiPath, { params: params }
        ).subscribe(response => {
            let original_data = response['data'];
            this._data.next(this.additionalDataProcessing(original_data));
            this.paginator.length = response['total'];
        })
    }

    load() {
        this._loaded = true;
        this.sort.sortChange.subscribe(newSort => {
            this._sortColumn = newSort['active'];
            this._sortDirection = newSort['direction'];
            this.updateData()
        })
        this.updateData();
    }

    connect(collectionViewer: CollectionViewer) { return this._data }

    disconnect(collectionViewer: CollectionViewer) { }

}
