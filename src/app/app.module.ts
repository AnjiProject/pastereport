import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material';
import { MatTableModule, MatPaginatorModule, MatFormFieldModule, MatInputModule, MatSortModule, MatSelectModule, MatGridListModule, MatButtonModule} from '@angular/material';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MomentModule } from 'ngx-moment'
// import { CalendarPipe, FromUnixPipe} from 'ngx-moment'

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ReportsComponent } from './reports/reports.component';
import { ReportService } from './report.service';
import { ReportGroupComponent } from './report-group/report-group.component'


@NgModule({
  declarations: [
    AppComponent,
    ReportsComponent,
    ReportGroupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    MatSelectModule,
    MatGridListModule,
    MatButtonModule,
    MomentModule
  ],
  providers: [ReportService],
  bootstrap: [AppComponent]
})
export class AppModule { }
