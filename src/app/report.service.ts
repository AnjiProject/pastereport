import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import * as moment from 'moment';

import { Report } from './models'
import { AbstractApiDataSource } from './datasources/base'


@Injectable()
export class ReportService extends AbstractApiDataSource<Report> {

  private _reportGroup: string = null;

  set reportGroup(reportGroup: string) {
        this._reportGroup = reportGroup;
        this.updateData();
  }

  get reportGroup(): string { return this._reportGroup; }

  getApiPath() {
      return 'api/v2/reports'
  }

  additionalParamsProcessing(params: HttpParams): HttpParams {
    if (this._reportGroup != null) {
      return params.set('reportGroup', this._reportGroup);
    }
    return params;
  }

  additionalDataProcessing(data: [Report]): [Report] {
      data.forEach(function(element){
          element.report_date = moment.unix(parseInt(element.report_date)).calendar();
      });
      return data;
  }

  getGroupList(){
      return this.http.get('/api/v2/report/groups');
  }

}