import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsComponent } from './reports/reports.component'
import { ReportGroupComponent } from './report-group/report-group.component'

const routes: Routes = [
    { path: '', component: ReportsComponent, pathMatch: 'full' },
    { path: 'group/:name', component: ReportGroupComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
