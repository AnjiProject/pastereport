import { Component, OnInit } from '@angular/core';
import { ReportService } from "../report.service"

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  reportGroups: [string];

  constructor(private reportService: ReportService) { }

  ngOnInit() {
      let self = this;
      this.reportService.getGroupList().subscribe(res => {
          self.reportGroups = res as [string]
      });
  }

}
