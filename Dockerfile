FROM alpine:3.7

# Install python

RUN apk add --no-cache python3 python3-dev gcc make musl-dev linux-headers nodejs-npm \
    && mkdir -p /app/prometheus /app/static \
    && npm install -g @angular/cli 

COPY requirements.txt /requirements.txt

RUN python3 -m pip install -r /requirements.txt
WORKDIR /app
COPY [".angular-cli.json", "tsconfig.json", "tslint.json", "package-lock.json", "package.json", "/app/"]

RUN npm install

COPY "pastereport" "/app/pastereport"
COPY "src" "/app/src"
RUN ng build -e prod

EXPOSE 8877

ENV UPLOAD_DIRECTORY=/app/static prometheus_multiproc_dir=/app/prometheus GUNICORN_WORKERS_COUNT=2 GUNICORN_BIND=127.0.0.1:8877

CMD ["/bin/sh", "-c", "/usr/bin/gunicorn -w ${GUNICORN_WORKERS_COUNT} -b ${GUNICORN_BIND} --capture-output --worker-class sanic.worker.GunicornWorker --log-level INFO pastereport.app:app"]
