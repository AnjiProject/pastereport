#!/usr/bin/env python3
import random
import click
import requests

from pastereport.app import app

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, KITWAY"
__credits__ = ["KITWAY"]
__license__ = "Proprietary"
__version__ = "0.6.1"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "b.gladyshev@kitway.com.ua"
__status__ = "Production"


@click.group()
def cli():
    pass


@cli.command()
@click.option('--host', default='0.0.0.0')
@click.option('--port', default=8877, type=int)
@click.option('--debug', default=False, is_flag=True)
@click.pass_context
def run(ctx, host, port, debug):
    app.run(host=host, port=port, debug=debug)


@cli.group()
def gendata():
    pass


@gendata.command()
@click.option('--count', default=5, type=int)
def reports(count):
    with open('pastereport/static/index.html') as test_file:
        test_data = ("\n".join(test_file.readlines())).encode('utf-8')
    for i in range(0, count):
        response = requests.post(
            'http://127.0.0.1:8877/paste/text/%s/' % random.choice([
                "load_data", "load_data_1", 'load_data_2'
            ]),
            data=test_data
        )
        print(response.text)


if __name__ == "__main__":
    cli()
