build:
	docker build -t registry.gitlab.com/anjiproject/pastereport:v0.7.1 .
push:
	docker push registry.gitlab.com/anjiproject/pastereport:v0.7.1
gunicorn:
	prometheus_multiproc_dir=./data/metrics gunicorn -w 2 -b 0.0.0.0:8877 --capture-output --worker-class sanic.worker.GunicornWorker --log-level INFO app:app
test-run:
	docker run -it --rm --net host --name test-pastereport registry.gitlab.com/anjiproject/pastereport:v0.7.1
clean:
	docker rm -f test-pastereport
check:
	pylint pastereport
	pycodestyle pastereport
	mypy pastereport --ignore-missing-imports